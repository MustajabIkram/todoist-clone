import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { DrawerExample } from "./Drawer";
import { FiHome } from "react-icons/fi";

export const Header = () => {
  return (
    <Box bg={"#db4c3f"}>
      <Flex h={16} alignItems={"center"} justifyContent={"left"}>
        <div>
          <DrawerExample />
        </div>
        <div style={{ marginLeft: "1%" }} color="white">
          <FiHome color="white" size={20} />
        </div>
        <div
          style={{
            marginLeft: "auto",
            marginRight: "2%",
            color: "white",
            fontSize: "30px",
          }}
        >
          +
        </div>
      </Flex>
    </Box>
  );
};
