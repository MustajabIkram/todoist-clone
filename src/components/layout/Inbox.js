import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import {
  GET_INBOX,
  GET_INBOX_SUCCESS,
  GET_INBOX_FAIL,
} from "../../redux/constants/constants";
import { configData, INBOX } from "./api/api";
import FormData from "form-data";
import {
  Center,
  Square,
  Circle,
  Editable,
  EditableInput,
  EditableTextarea,
  EditablePreview,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuItemOption,
  MenuGroup,
  MenuOptionGroup,
  MenuDivider,
  Checkbox,
  CheckboxGroup,
  VStack,
  HStack,
  StackDivider,
} from "@chakra-ui/react";
import { BsThreeDots, BsPlusCircle, BsPlusLg } from "react-icons/bs";

import { FiEdit3 } from "react-icons/fi";
import { Text } from "@chakra-ui/react";
import { Box } from "@chakra-ui/react";
import { AiOutlineDelete } from "react-icons/ai";
import { Button, ButtonGroup, IconButton } from "@chakra-ui/react";

import EditModal from "./EditModal";
import { clicked } from "./Drawer";

class Inbox extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    const { inbox, loading, error } = this.props;
    return (
      <div>
        {loading && <div>Loading . . .</div>}
        {error && <div>{error.message}</div>}
        {/* {inbox.map((inbox) => (
          <h3 key={inbox.id}>{inbox.content}</h3>
        ))} */}
        <Center>
          <VStack>
            <HStack>
              {/* {this.state.project.name ? (
            <Editable defaultValue={projectName}>
            <EditablePreview />
              <EditableInput />
              </Editable>
          ) : null} */}

              <Menu>
                <MenuButton
                  as={IconButton}
                  aria-label="Options"
                  icon={<BsThreeDots />}
                  variant="outline"
                />
                <MenuList>
                  <MenuItem icon={<FiEdit3 />} command="⌘T">
                    Edit Project
                  </MenuItem>
                  <MenuItem icon={<AiOutlineDelete />} command="⌘⇧N">
                    Delete Project
                  </MenuItem>
                </MenuList>
              </Menu>

              {/* <Button rightIcon={<BsThreeDots />} p="10px"/> */}
            </HStack>
            <VStack
              divider={<StackDivider borderColor="gray.200" width="40vw" />}
              spacing={4}
              align="stretch"
            >
              {inbox.map((task) => (
                <HStack justifyContent="space-between">
                  <HStack justfyContent="flex-start">
                    <Checkbox key={task.id} colorScheme="red"></Checkbox>

                    <Text fontSize="lg" justifySelf="flex-end">
                      {task.content}
                    </Text>
                  </HStack>
                  <Box justifySelf="end">
                    <EditModal task={task}></EditModal>
                  </Box>
                </HStack>
              ))}
              <Button
                w="100px"
                p="10px"
                leftIcon={<BsPlusCircle />}
                colorScheme="red"
                variant="outline"
              >
                Add task
              </Button>
            </VStack>
          </VStack>
        </Center>
      </div>
    );
  }
}

const mapStateToProps = ({ inboxReducer }) => {
  console.log(inboxReducer);
  return {
    inbox: inboxReducer.inbox,
    loading: inboxReducer.loading,
    error: inboxReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: () => {
    dispatch({ type: GET_INBOX });
    axios(configData("get", INBOX))
      .then((res) => {
        dispatch({ type: GET_INBOX_SUCCESS, payload: res.data });
      })
      .catch((err) => {
        dispatch({ type: GET_INBOX_FAIL, payload: err });
      });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Inbox);
