import React from "react";
import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerContent,
  DrawerCloseButton,
  Button,
  Input,
  useDisclosure,
} from "@chakra-ui/react";

import { RxHamburgerMenu } from "react-icons/rx";
import { VscInbox } from "react-icons/vsc";
import { SlCalender } from "react-icons/sl";

import { Favourites } from "./Favourites";
import { Link } from "react-router-dom";

export let clicked = true;

export function DrawerExample({ children, ...rest }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();

  return (
    <>
      <Button
        ref={onOpen || btnRef}
        colorScheme="white"
        onClick={() => {
          console.log("clicked");
          if (!clicked) {
            onOpen();
            clicked = true;
          } else {
            onClose();
            clicked = false;
          }
        }}
      >
        <RxHamburgerMenu size={"20px"} />
      </Button>
      <Drawer
        variant="alwaysOpen"
        {...rest}
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        trapFocus={false}
        closeOnOverlayClick={false}
        blockScrollOnMount={false}
      >
        <DrawerContent marginTop={16} style={{ backgroundColor: "#fafafa" }}>
          <DrawerBody marginTop={5}>
            <Link to="/inbox">
              <div style={{ display: "flex", alignItems: "center" }}>
                <VscInbox />
                <p style={{ marginLeft: "10px" }}>Inbox</p>
              </div>
            </Link>
            <Link to="/today">
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  marginTop: "10px",
                }}
              >
                <SlCalender />
                <p style={{ marginLeft: "10px" }}>Today</p>
              </div>
            </Link>
            {/* Accordion will come here */}
            <div
              style={{
                marginTop: "20px",
              }}
            >
              <Favourites />
            </div>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}
