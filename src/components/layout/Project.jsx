import React, { Component } from "react";
import axios from "axios";
import FormData from "form-data";
import { Center, Square, Circle } from "@chakra-ui/react";
import {
  Editable,
  EditableInput,
  EditableTextarea,
  EditablePreview,
} from "@chakra-ui/react";
import { Checkbox, CheckboxGroup } from "@chakra-ui/react";
import { VStack, HStack, StackDivider } from "@chakra-ui/react";
import { checkboxTheme } from "../../App";
import { BsThreeDots, BsPlusCircle, BsPlusLg } from "react-icons/bs";

import { FiEdit3 } from "react-icons/fi";
import { Text } from "@chakra-ui/react";
import { Box } from "@chakra-ui/react";
import { AiOutlineDelete } from "react-icons/ai";
import { Button, ButtonGroup, IconButton } from "@chakra-ui/react";
import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuItemOption,
  MenuGroup,
  MenuOptionGroup,
  MenuDivider,
} from "@chakra-ui/react";
import EditModal from "./EditModal";
import EditableText from "./EditableText";

class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project: {},
      tasks: [],
    };
  }

  componentDidMount() {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: "https://api.todoist.com/rest/v2/tasks?project_id=2307149255",
      headers: {
        Authorization: "Bearer 7b61f23db69ae44bdde323ab80888aaad482eec1",
      },
    };

    axios(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        // const tasks1 = JSON.stringify(response.data);
        const tasks = response.data;
        this.setState({ tasks });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  render() {
    console.log("w", this.state);
    const projectName = this.state.tasks.name;
    return (
      <Center p="10vw">
        <VStack>
          <HStack>
            {/* {this.state.project.name ? (
            <Editable defaultValue={projectName}>
            <EditablePreview />
              <EditableInput />
              </Editable>
          ) : null} */}

            <Menu>
              <MenuButton
                as={IconButton}
                aria-label="Options"
                icon={<BsThreeDots />}
                variant="outline"
              />
              <MenuList>
                <MenuItem icon={<FiEdit3 />} command="⌘T">
                  Edit Project
                </MenuItem>
                <MenuItem icon={<AiOutlineDelete />} command="⌘⇧N">
                  Delete Project
                </MenuItem>
              </MenuList>
            </Menu>

            {/* <Button rightIcon={<BsThreeDots />} p="10px"/> */}
          </HStack>
          <VStack
            divider={<StackDivider borderColor="gray.200" width="70vw" />}
            spacing={4}
            align="stretch"
          >
            {this.state.tasks.length
              ? this.state.tasks.map((task) => (
                  <HStack justifyContent="space-between">
                    <HStack justifyContent="flex-start">
                      <Checkbox key={task.id} colorScheme="red"></Checkbox>

                      <Text fontSize="lg">{task.content}</Text>
                    </HStack>
                    <Box>
                      <EditModal task={task}></EditModal>
                      
                    </Box>
                  </HStack>
                ))
              : null}
            <Button
              w="100px"
              p="10px"
              leftIcon={<BsPlusCircle />}
              colorScheme="red"
              variant="outline"
            >
              Add task
            </Button>
          </VStack>
        </VStack>
      </Center>
    );
  }
}

export default Project;
