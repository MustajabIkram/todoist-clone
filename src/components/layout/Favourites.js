import React from "react";

import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
} from "@chakra-ui/react";

export function Favourites() {
  return (
    <Accordion defaultIndex={[0]} allowMultiple>
      <AccordionItem
        style={{
          marginTop: "40px",
          border: "none",
          color: "#555",
        }}
      >
        <h2>
          <AccordionButton>
            <Box
              as="span"
              flex="1"
              textAlign="left"
              style={{ fontWeight: "bold" }}
            >
              Favourites
            </Box>
            <AccordionIcon />
          </AccordionButton>
        </h2>
        <AccordionPanel pb={4}>Random Project</AccordionPanel>
      </AccordionItem>

      <AccordionItem
        style={{ marginTop: "20px", border: "none", color: "#555" }}
      >
        <h2>
          <AccordionButton>
            <Box
              as="span"
              flex="1"
              textAlign="left"
              style={{ fontWeight: "bold" }}
            >
              Projects
            </Box>
            <AccordionIcon />
          </AccordionButton>
        </h2>
        <AccordionPanel pb={4}>Some Projects</AccordionPanel>
        <AccordionPanel pb={4}>Some Projects</AccordionPanel>
        <AccordionPanel pb={4}>Some Projects</AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
}
