import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/react'
import { Button } from "@chakra-ui/react";
import { useDisclosure } from '@chakra-ui/react';
import React from 'react';
import { FiEdit3 } from "react-icons/fi";
import { CalendarIcon } from '@chakra-ui/icons'
import { Text } from '@chakra-ui/react';
import EditableText from './EditableText';


// const VerticallyCenter = props => {
//   const { isOpen, onOpen, onClose } = useDisclosure()
//   console.log(props)
//   return (
//     <>
//       <Button onClick={onOpen}> <FiEdit3 /> </Button>

//       <Modal onClose={onClose} isOpen={isOpen} isCentered>
//         <ModalOverlay />
//         <ModalContent>
//           <ModalHeader></ModalHeader>
//           <ModalCloseButton />
//           <ModalBody>
//           </ModalBody>
//           <ModalFooter>
//             <Button onClick={onClose}>Close</Button>
//           </ModalFooter>
//         </ModalContent>
//       </Modal>
//     </>
//   )
// }
// export default VerticallyCenter

// 

const EditModal = props => {
  const OverlayOne = () => (
    <ModalOverlay
      bg='blackAlpha.300'
      backdropFilter='blur(10px) hue-rotate(90deg)'
    />
  )

  const { isOpen, onOpen, onClose } = useDisclosure()
  const [overlay, setOverlay] = React.useState(<OverlayOne />)

  // console.log(props)

  return (
    <>
      <Button
        onClick={() => {
          setOverlay(<OverlayOne />)
          onOpen()
        }}
      >
        <FiEdit3 />
      </Button>
      <Modal isCentered size="xl" isOpen={isOpen} onClose={onClose}>
        {overlay}
        <ModalContent>
          <ModalHeader>
            <EditableText taskName={props.task.content}/>
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>

            <Button
              w="100px"
              p="10px"
              rightIcon={<CalendarIcon />}
              colorScheme="green"
              variant="outline"
            >
              Due Date
            </Button>

          </ModalBody>
          <ModalFooter>
            <Button onClick={onClose}>Close</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}


export default EditModal
