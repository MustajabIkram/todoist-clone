import React from "react";
import { InfoIcon, CalendarIcon, ChevronDownIcon } from "@chakra-ui/icons";

export const Sidebar = () => (
  <div className="sidebar">
    <ul className="sidebar_generic">
      <li>
        <span>
          <InfoIcon />
        </span>
        <span>Inbox</span>
      </li>
      <li>
        <span>
          <CalendarIcon />
        </span>
        <span>Today</span>
      </li>
    </ul>
    <div className="sidebar_middle">
      <span>
        <ChevronDownIcon />
        <h2>Projects</h2>
      </span>
    </div>
    <ul className="sidebar_projects">Projects Will Be Here</ul>
    Add Project components
  </div>
);
