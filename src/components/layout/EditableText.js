import {
    Editable,
    EditableInput,
    EditableTextarea,
    EditablePreview,
} from '@chakra-ui/react'
import { Button, ButtonGroup, IconButton } from "@chakra-ui/react";
import { useEditableControls } from '@chakra-ui/react'
import { EditIcon, CheckIcon, CloseIcon } from '@chakra-ui/icons';
import { Input } from '@chakra-ui/react';
import { Flex } from '@chakra-ui/react';


const EditableText = (props) => {
    /* Here's a custom control */
    console.log("nice", props)
    function EditableControls() {
        const {
            isEditing,
            getSubmitButtonProps,
            getCancelButtonProps,
            getEditButtonProps,
        } = useEditableControls()

        return isEditing ? (
            <ButtonGroup justifyContent='center' size='sm'>
                <IconButton ml="10px" icon={<CheckIcon />} {...getSubmitButtonProps()} />
                <IconButton icon={<CloseIcon />} {...getCancelButtonProps()} />
            </ButtonGroup>
        ) : (
            <Flex justifyContent='center'>
                <IconButton size='sm' icon={<EditIcon />} {...getEditButtonProps()} />
            </Flex>
        )
    }

    return (
        <Editable
            textAlign='center'
            defaultValue={props.taskName}
            fontSize='xl'
            isPreviewFocusable={false}
        >
            <EditablePreview  />
            {/* Here is the custom input */}
            <Input w="50%"  as={EditableInput} />
            <EditableControls  />
        </Editable>
    )
}

export default EditableText