export const INBOX = "tasks?project_id=2307145352";

export const configData = (method, path) => ({
  method,
  maxBodyLength: Infinity,
  url: `https://api.todoist.com/rest/v2/${path}`,
  headers: {
    Authorization: "Bearer 7b61f23db69ae44bdde323ab80888aaad482eec1",
  },
});
