import {
  GET_INBOX,
  GET_INBOX_SUCCESS,
  GET_INBOX_FAIL,
} from "../constants/constants";

const initialState = {
  inbox: [],
  loading: false,
  error: null,
};

const inboxReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_INBOX:
      return {
        ...state,
        loading: true,
      };
    case GET_INBOX_SUCCESS:
      console.log("this", action.payload);
      return {
        ...state,
        inbox: action.payload,
        loading: false,
      };
    case GET_INBOX_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default inboxReducer;
