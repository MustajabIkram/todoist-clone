import { combineReducers } from "redux";
import inboxReducer from "./reducer";

export default combineReducers({
  inboxReducer,
});
