import React from "react";
import { BrowserRouter, Switch, Route, Link, Routes } from "react-router-dom";

import "./App.css";
import { Header } from "./components/layout/Header";
import Inbox from "./components/layout/Inbox";
import { Today } from "./components/layout/Today";
import Project from "./components/layout/Project"
function App() {
  return (
    <BrowserRouter>
      {/* <Header /> */}
      <Switch>
        <Route exact path="/" component={Project}/>
        {/* <Route exact path="/" component={Projects} />
        <Route exact path="/projects" component={Projects} />
        <Route path="/today" component={Today} /> */}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
